var express = require('express'),
    mongoose = require('mongoose'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    database = require('./config/database');

mongoose.connect(database.url);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error...'));
db.once('open', function callback() {
    console.log('testdb opened');
});

/*config*/
var app = express();

app.use(express.static(__dirname + '/public'));

app.use(logger('dev'));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

app.use(methodOverride());

require('./server/routes.js')(app);

var server = app.listen(3030, function() {

    console.log("App listening on port 3030");
});

var io = require('socket.io').listen(server);
require('./server/core/server-socket')(io);
