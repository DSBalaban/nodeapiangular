module.exports = function(io) {

    var x = 0;
    io.on('connection', function(socket) {
        console.log(++x);
        console.log(socket.id);

        socket.on('disconnect', function() {

            io.emit('user disconnected');
        });

        socket.on('add user', function() {
            io.emit('connected', socket.id);
        });

        socket.on('data changed', function(data) {

            console.log("Data change log");
            io.emit('data update', data);
        })
    });
};