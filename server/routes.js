var Todo = require('./models/Todo');

module.exports = function(app) {

    app.get('/api/todos', function(req, res) {

        Todo.find(function(err, data) {

            if(err) {

                res.send(err);
            }

            res.send(data);
        });
    });

    app.post('/api/todos', function(req, res) {

        Todo.create({

            text: req.body.text,
            done: false
        }, function(err, data) {

            if(err) {

                res.send(err);
            }

            Todo.find(function(err, data) {

                if(err) {

                    res.send(err);
                }

                res.json(data);
            });
        });
    });

    app.delete('/api/todos/:todo_id', function(req, res) {

        Todo.remove({

            _id: req.params.todo_id
        }, function(err) {

            if(err) {

                res.send(err);
            }

            Todo.find({}, function(err, data) {

                if(err) {

                    res.send(err);

                }

                res.json(data);
            });
        });
    });

    app.get('*', function(req, res) {

        res.sendfile('./public/index.html');
    });
};