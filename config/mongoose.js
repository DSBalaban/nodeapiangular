var mongoose = require('mongoose');

module.exports = function(databaseUrl) {

    mongoose.connect(databaseUrl);
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error...'));
    db.once('open', function callback() {
        console.log('testdb opened');
    });

};