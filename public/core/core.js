app.controller('mvMainController', function($scope, $http, socket) {

    socket.emit('add user');

    socket.on('connected', function(id) {

        console.log("Socket " + id + " connected");
    });

    socket.on('user disconnected', function() {

        console.log("User DC'd");
    });

    socket.on('data update', function(newData) {

        console.log("data update log", newData);
        $scope.todos = newData;

    });

    $scope.formData = {};

    $http.get('/api/todos')
        .success(function(data) {

            $scope.todos = data;
            console.log(data);
        })
        .error(function(err) {

            console.log(err);
        });

    $scope.createTodo = function() {

        $http.post('/api/todos', $scope.formData)
            .success(function(data) {

                $scope.formData = {};

                socket.emit('data changed', data);

            })
            .error(function(err) {

                console.log(err);
            });
    };

    $scope.deleteTodo = function(id) {

        $http.delete('/api/todos/' + id)
            .success(function(data) {

                socket.emit('data changed', data);
            })
            .error(function(err) {

                console.log(err);
            });
    };
});